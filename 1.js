const savedColor = localStorage.getItem('background-color'); //збергіаємо в змінну інфо про колір фону в LocalStorage
if (savedColor) {
  document.body.style.backgroundColor = savedColor;
} else {
  document.body.style.backgroundColor = 'rgb(223 237 198)'; // Створюємо початковий колір фону
}

const fragment = document.createDocumentFragment();

const input = document.createElement("input"); //додаємо кнопку яка буде змінювати тему
input.type = "button";
input.value = "Змінити тему";
input.onclick = function() { // додаємо обробник по кліку миші і створюємо функцію яка буде змінювати колір теми , та чистити localStorage
  if (document.body.style.backgroundColor === 'rgb(175, 164, 164)') {
    localStorage.clear();
    document.body.style.backgroundColor = 'rgb(223 237 198)';
  } else {
    document.body.style.backgroundColor = 'rgb(175, 164, 164)'; // Робимо новий колір фону
    localStorage.setItem('background-color', 'rgb(175, 164, 164)'); // зберігаємо новий колір фону в localStorage
  }
};
fragment.appendChild(input); //додаємо фрагмент на сторінку
document.body.appendChild(fragment);
